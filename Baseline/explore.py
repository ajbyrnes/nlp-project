#!/usr/bin/env python3

import json

tfile = "data v1.0/train-v1.0.json"
dfile = "data v1.0/dev-v1.0.json"

with open(tfile, "r") as TF:
    train = TF.read()

train = json.loads(train)

# train.keys() = ["data", "version"]
# train["data"] = articles; list of dictionaries
# train["version"] = version number; string

# article.keys() = ["paragraphs", "title"]
# article["paragraphs"] = sections; list of dictionaries 
# article["title"] = article title; string

# paragraph.keys() = ["context", "qas"]
# paragraph["context"] = text of paragraph; string 
# paragraph["qas"] = question-answer data; list of dictionaries

# qa.keys() = ["answers", "id", "question"]
# qa["answers"] = answers; list of dictionaries
# qa["id"] = unique answer id; string
# qa["question"] = text of question; string

# answer.keys() = ["answer_start", "text"]
# answer["answer_start"] = position of start of answer; int 
# answer["text"] = answer text; string

for article in train["data"]:
    print(article["title"])
    
    for paragraph in article["paragraphs"]:
        print(paragraph["context"])
        print()

        for qa in paragraph["qas"]:
            for answer in qa["answers"]:
                print(qa["question"], answer["text"])
       
        print()

    break
