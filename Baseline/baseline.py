#!/usr/bin/env python3

import json
import nltk
import math

from baseline_utils import *

tfile = "data v1.0/train-v1.0.json"
dfile = "data v1.0/dev-v1.0.json"


if __name__ == "__main__":

    #with open(tfile, "r") as TF:
    #    data = TF.read()
 
    with open(dfile, "r") as DF:
        data = DF.read()
    
    data = json.loads(data)

    tokenizer = nltk.data.load("tokenizers/punkt/english.pickle")
    
    predictions = {}

    #answer_lengths = []        # TRAINING
    #words_per_answer = []

    for article in data["data"]:
        for paragraph in article["paragraphs"]:
            context = paragraph["context"]
            words = set(nltk.word_tokenize(context))        # All words occuring in context
            sentences = tokenizer.tokenize(context)         # All sentences occuring in context
            
            #print(context)
            C, IC = counts(context)

            # Get vectors - counts of each possible word occuring in the sentence
            sentence_vectors = [vectorize(sentence, words) for sentence in sentences]
          
            
            for qa in paragraph["qas"]:
                question = qa["question"]
                question_vector = vectorize(question, words)        # Count of each possible word in question
                
                s_prediction = sentence_prediction(question_vector, sentence_vectors)
                
                a_prediction = sliding_window(C, IC, context, question, sentences[s_prediction])
                
                predictions[qa["id"]] = a_prediction
                #print(question)
                #print(sentences[s_prediction])
                #print(a_prediction)

                #print()
                #words_per_answer.append(len(nltk.word_tokenize(qa["answers"][0]["text"])))

            #break   # Stop after first paragraph - for testing only
        #break # Stop after first article - for testing only
    
    pfile = "predictions.json"
    with open(pfile, "w+") as PF:
        json.dump(predictions, PF)

    # TRAINING
    #average = sum(words_per_answer) / len(words_per_answer)
    #median = sorted(words_per_answer)[math.floor(len(words_per_answer) / 2)]
    #average = sum(answer_lengths) / len(answer_lengths)
    #median = sorted(answer_lengths)[math.floor(len(answer_lengths) / 2)]
    #print(average, median)
