Baseline:
    Contains all code to run and generate predictions from the baseline.
    Contains pre-generated predictions from baseline.
    
BERT:
    Contains predictions from Bert-Base, Bert-Large, Uncased, and Bert-Large, Cased
    Pre-trained models and training scripts for BERT are found at:
        https://github.com/google-research/bert
        
TriviaQA:
    Contains TriviaQA wikipedia training data converted to SQuAD format, with non-answered questions removed
    Contains scripts to obtain that data from the TriviaQA training data
    Contains run_squad_trivia.py, modified from Bert SQuAD training program run_squad. Should train model
    for an additional epoch on TriviaQA data, but was unable to test.